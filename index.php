<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
include_once  folder('/functions/data_connector/db-connector.php');
include_once folder('/functions/login-register/function-register.php');
include_once folder('/functions/newsletter/newsletter.php'); 
require_once folder('/views/layouts/header.php');

require_once folder('/views/contents/caroussel.php'); 
require_once folder('/views/contents/product.php');
require_once folder('/views/contents/blog.php');

require_once folder('/views/layouts/footer.php');
