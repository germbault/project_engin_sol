<?php
namespace App\Helpers;

class Text {
    // Permet de couper la chaine de caratère à 60 pour le blog
    public static function excerpt(string $content, int $limit = 150)
    {
        if (mb_strlen($content) <= $limit) {
            return $content;
        }
        // Commence à couper de 0 jusqu'à la limit, si la chaine est trop grande
        // Et permet de couper à la fin du mot avec $lastSpace
        $lastSpace = mb_strpos($content, ' ', $limit);
        return mb_substr($content, 0, $lastSpace). '...';
    }
}