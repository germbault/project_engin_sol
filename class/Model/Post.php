<?php
namespace App\Model;
use App\Helpers\Text;

class Post {

    private $id;

    private $slug;

    private $title;

    private $content;

    private $created_at;

    private $categories = [];

    // Création de méthode pour récupérer les variables qui sont en private
    // Se qui s'appelle des getters et permet de typer le retour ex: ?string

    public function getTitle (): ?string
    {
        return $this->title;
    }

    public function getFormattedContent () : ?simplexml_load_string
    {
        return nl2br(htmlentities($this->content));
    }

    public function getExcerpt (): ?string
    {
        if ($this->content === null) {
            return null;
        }
        return nl2br(htmlentities(Text::excerpt($this->content, 150)));
    }

    public function getCreatedAt (): \DateTime
    {
        return new \DateTime($this->created_at);
    }

    public function getSlug (): ?string
    {
        return $this->slug;
    }

    public function getID (): ?int // return soit null ou un entier
    {
        return $this->id;
    }
}