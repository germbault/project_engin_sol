<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php'); 
    // include_once  folder('/functions/function-register.php');
    
    include_once  folder('/vendor/autoload.php');
    use App\Helpers\Text;
    use App\Model\Post;

    // Requête pour récuprer les 12 articles de la bd
    $conn = connect();
    $query = $conn->query('SELECT * FROM post ORDER BY created_at DESC LIMIT 12');
    $posts = $query->fetchAll(PDO::FETCH_CLASS, Post::class);

?>
<section id="blog">
    <div class="container">
        <div class="row top">
            <img class="pattern-left" src="/img/svg/pattern.svg" alt="motif">
            <h3>Blog</h3>
            <img class="pattern-right" src="/img/svg/pattern.svg" alt="motif">
        </div>
        <p class="tx-middle">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
        </p>
        <div class="card-flex">
            <?php foreach($posts as $post): ?>            
                <div class="card-row">
                    <div class="card">
                        <h5 class="card-title"><?= htmlentities($post->getTitle()) ?></h5>
                        <p class="card-date"><?= $post->getCreatedAt()->format('d F Y') ?></p>
                        <p><?= $post->getExcerpt() ?></p>
                        <p class="top-buttom">                            
                        <a href="/views/layouts/elements/article.php?id=<?= $post->getID() ?>" class="blog-buttom">Voir plus</a>
                        </p>
                    </div>
                </div>            
        <?php endforeach ?>
        </div>
    </div>
</section>
