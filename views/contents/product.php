<?php
    include_once  folder('/views/layouts/elements/modal-form.php');
?>

<section id="product" class="product-bg">
    <div class="container">
        <div class="row top">
            <img class="pattern-left" src="/img/svg/pattern.svg" alt="motif">
            <h3>Produit</h3>
            <img class="pattern-right" src="/img/svg/pattern.svg" alt="motif">
        </div>
        <p class="tx-middle">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
        </p>
        <div class="shop">
            <div class="row">
                <div class="width50 fade">
                    <div>
                        <img src="/img/gallerie/01.png" alt="image">
                    </div>
                    <div class="banner-row banner-bg">
                        <p class="color02">1250.99$</p>
                        <span class="ajust"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                        <i class="fas fa-star-half-alt"></i><i class="far fa-star"></i></span>
                    </div>
                </div>
                <div class="width50">
                    <h2>La version de base</h2>
                    <div>
                        <ul>
                            <li>Bonne pour deux utilisations. 
                                Elle attire 50 % des molécule atomique 15 pied autour de l'utilisateur.</li>
                            <li> Elle est blanche (tout le monde sait que la couleur ça coute cher) </li>
                            <li> Pour plus de sécurité elle est impossible à modifier. Si la boite est  ouverte elle explose.</li>
                        </ul>
                    </div>
                    <div class="svg-wrapper i" onclick="document.getElementById('activator').style.display='block'" style="width:auto;">                                            
                        <img class="shape" src="/img/svg/bottom.svg" alt="logo">                                       
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="width50 fade">
                    <div>
                        <img src="/img/gallerie/02.png" alt="image">
                    </div>
                    <div class="banner-row banner-bg">
                        <p class="color02">3050.74$</p>
                        <span class="ajust"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                        <i class="fas fa-star-half-alt"></i><i class="far fa-star"></i></span>
                    </div>
                </div>
                <div class="width50">
                    <h2>La version de pas base</h2>
                    <div>
                        <ul>
                            <li>Bonne pour deux utilisations. 
                                Elle attire 25 % des molécule atomique 10 pied  autour de l'utilisateur.</li>
                            <li>Pour plus de sécurité elle est trempée dans un  plastique qui la rend monobloc et impénétrable.</li>
                            <li>Elle est de couleur pastel.</li>
                        </ul>
                    </div>
                    <div class="svg-wrapper1 i" onclick="document.getElementById('activator').style.display='block'" style="width:auto;">                                            
                        <img class="shape1" src="/img/svg/bottom.svg" alt="logo">                                       
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="width50 fade">
                    <div>
                        <img src="/img/gallerie/03.png" alt="image">
                    </div>
                    <div class="banner-row banner-bg">
                        <p class="color02">10287.23$</p>
                        <span class="ajust"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                        <i class="fas fa-star-half-alt"></i><i class="far fa-star"></i></span>
                    </div>
                </div>
                <div class="width50">
                    <h2>La version de gros gros luxe</h2>
                    <div>
                        <ul>
                            <li>Bonne pour deux utilisations.</li> 
                            <li>Elle n'attire aucune molécule nocive.</li>
                            <li>Pour plus de sécurité elle est trempée dans un plastique qui la rend monobloc et 
                                impénétrable qui elle est recouverte d’une couche d’acier pour plus d’impénétrabilité.</li>
                            <li>De plus une couverture de fonte permet de ne pas le perdre car au poids qu'il a vous ne 
                                pouvez l'oublier! (Plus c'est lourd plus ça vaut cher non?).</li>
                            <li>Elle a deux choix de couleurs vive (tout le monde sait que la couleur ça coute cher) Rouge ou brun vif.</li>
                        </ul>
                    </div>
                    <div class="svg-wrapper2 i" onclick="document.getElementById('activator').style.display='block'" style="width:auto;">                                            
                        <img class="shape2" src="/img/svg/bottom.svg" alt="logo">                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


