<section id="carrousel">
    <div class="container">
        <h1 class="title">Sub-discombobulateur Atomique !!</h1>
        <div class="row">
            <img class="pattern-left" src="/img/svg/pattern.svg" alt="motif">
            <h3>Qui somme nous</h3>
            <img class="pattern-right" src="/img/svg/pattern.svg" alt="motif">
        </div>
        <p class="tx-middle">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
        </p>
        <div class="gallery shop">
            <div id="caroussel">                               
                <div id="slide1" class="slide-gallery">                                
                    <div class="visual-gallery">                                
                        <img src= "/img/gallerie/01.png">                                    
                    </div>
                    <div class="info-text">                                                                
                        <p>Sub-discombobulateur Atomique <strong>VERSION DE BASE</strong></p>                                                                                                                                  
                    </div>                                                          
                </div> 

                <div id="slide2" class="slide-gallery">                                
                    <div class="visual-gallery">                                
                        <img src= "/img/gallerie/02.png">                                    
                    </div>
                    <div class="info-text">                                                                
                        <p>Sub-discombobulateur Atomique <strong>VERSION PAS DE BASE</strong></p>                                                                                                                                  
                    </div>                                                          
                </div>

                <div id="slide3" class="slide-gallery">                                
                    <div class="visual-gallery">                                
                        <img src= "/img/gallerie/03.png">                                    
                    </div>
                    <div class="info-text">                                                                
                        <p>Sub-discombobulateur Atomique <strong>VERSION DE GROS GROS LUXE</strong></p>                                                                                                                                  
                    </div>                                                          
                </div>
            </div>
                                              
        </div>
        <a href="#blog" class="scroll-down">Scrool down &nbsp<i class="fas fa-long-arrow-alt-down"></i></a> 
    </div>
</section>


