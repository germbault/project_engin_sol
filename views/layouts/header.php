<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!doctype html>
<html class="no-js" lang="fr">

<head>
    <meta charset="utf-8">
    <title>Engin sol</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"></link>

    <meta name="theme-color" content="#fafafa">
</head>

<body>
    <div class="site-container">
        <div class="site-pusher">
            <header>
                <div class="container">
                <div class="user-link">
                    <?php if (isset($_SESSION['auth'])): ?>
                            <span class="color-02">Bienvenue, <strong>(<?php echo $_SESSION["pseudo"]; ?>)</strong></span>
                            <a href="/admin/dashboard.php"><i class="fas fa-th-list"></i>Mon compte&nbsp&nbsp-</a>
                            <a href="/views/layouts/elements/logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a>                                                                                  
                    <?php else: ?>
                            <a href="/views/layouts/elements/login.php"><span><i class="fas fa-user-lock"></i>S'identifier -</span></a>
                            <a href="/views/layouts/elements/register.php"><span><i class="fas fa-user-plus"></i>S'inscrire</span></a>
                    <?php endif; ?>
                </div>
                    <div class="row header-row">
                        <div class="logo">
                            <div class="wrap">                
                                <a href="#">
                                    <img class="size-logo" src="/img/svg/logo.svg" alt="logo">
                                </a>              
                            </div>
                        </div>
                        <div class="menu_burger">
                            <a href="#" class="header__icon" id="header__icon"></a>
                        </div>
                        <div>
                            <nav class="col-01">
                                <ul class="menu">                
                                    <li class="remove-cache"><a href="#carrousel">Accueil</a></li>
                                    <li class="menu-lign remove-cache"><a href="#product">Produit</a></li>
                                    <li class="remove-cache"><a href="#blog">Blog</a></li>
                                </ul>
                            </nav>
                                                   
                            <form id="mailing" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            <?php
                                if (isset($error2)) { echo "<div class='error-php'>" . $error2 . "</div>"; }
                                
                                if (isset($msgSuccess2)) { echo "<div class='succes-php'>" . $msgSuccess2 . "</div>"; }
                            ?>
                                <label class="input_wrap" for="mailing">                       
                                    <input id=email name=mailing type=email placeholder="exemple@gmail.com" class="input-form" value="">
                                </label>                           
                                <button class="style-bottom" name="formNewsletter" type="submit">Infolettre</button>
                            </form>                           
                        </div>
                    </div>
                </div>
            </header>
            <main>
