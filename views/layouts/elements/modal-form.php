<section id="activator" class="modal">
    <form class="modal-content animate">
        <div class="close">
            <span onclick="document.getElementById('activator').style.display='none'" title="Close Modal">&times;</span>
        </div>
        <div>
            <div class="container-form">
                <p class="font-size1"><strong>!! IMPORTANT !!</STRONG></P>
                <p class="font-size2">Pour réserver se produit, vous devez vous inscrire à l'infolettre et reçevoir une notification
                    dès qu'il sera disponible. Les détails de la réservation vous permettera de visualiser la date ainsi que le produit commandé.</p>
            </div>
        </div>
        <div class="container-form">
            <button type="button" onclick="document.getElementById('activator').style.display='none'"
            class="cancelbtn">Revenir</button>
        </div>
    </form>
</section>