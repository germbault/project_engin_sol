<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php'); 
    include_once folder('/functions/login-register/function-register.php');
    include_once folder('/functions/newsletter/newsletter.php');

    // Permet d'afficher la totalité de l'article à partir du lien dans une nouvelle page
    if (isset($_GET['id']) AND !empty($_GET['id'])) {
        $get_id = htmlspecialchars($_GET['id']);

        $conn = connect();
        $article = $conn->prepare("SELECT * FROM post WHERE id = ?");
        $article->execute(array($get_id));

        if ($article->rowcount() == 1) {
            $article = $article->fetch();
            $title = $article['title'];
            $slug = $article['slug'];
            $content = $article['content'];
        }else {
            die("Cet article n'existe pas !");
        }
    }else {
        die('Erreur');
    }
?>
<!doctype html>
<html class="no-js" lang="fr">

<head>
    <meta charset="utf-8">
    <title>Engin sol</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"></link>

    <meta name="theme-color" content="#fafafa">
</head>

<body>
    <div class="site-container">
        <div class="site-pusher">
            <header>
                <div class="container">
                <div class="user-link">
                    <?php if (isset($_SESSION['auth'])): ?>
                            <span class="color-02">Bienvenue, <strong>(<?php echo $_SESSION["pseudo"]; ?>)</strong></span>
                            <a href="/admin/dashboard.php"><i class="fas fa-th-list"></i>Mon compte&nbsp&nbsp-</a>
                            <a href="/views/layouts/elements/logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a>
                                                        
                                <!-- <?php if ($_SESSION['admin'] == 1) : ?>                         
                                    <li class="edit-profil"><a href="/includes/view-blog/post/panel.php"><span><i class="fas fa-user-cog"></i>Administrateur</span></a></li>
                                <?php endif; ?> -->
                    <?php else: ?>
                    <a href="/views/layouts/elements/login.php"><span><i class="fas fa-user-lock"></i>S'identifier -</span></a>
                    <a href="/views/layouts/elements/register.php"><span><i class="fas fa-user-plus"></i>S'inscrire</span></a>
                    <?php endif; ?>
                </div>
                    <div class="row header-row">
                        <div class="logo">
                            <div class="wrap">                
                                <a href="">
                                    <img class="size-logo" src="/img/svg/logo.svg" alt="logo">
                                </a>              
                            </div>
                        </div>
                        <div class="menu_burger">
                            <a href="#" class="header__icon" id="header__icon"></a>
                        </div>

                        <div>
                            <nav class="col-01">
                                <ul class="menu">                
                                    <li class="remove-cache"><a href="/index/#carrousel">Accueil</a></li>
                                    <li class="menu-lign remove-cache"><a href="/index/#product">Produit</a></li>
                                    <li class="remove-cache"><a href="/index/#blog">Blog</a></li>
                                </ul>
                            </nav>
                            
                            <form id="mailing" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            <?php
                                if (isset($error2)) { echo "<div class='error-php'>" . $error2 . "</div>"; }
                                
                                if (isset($msgSuccess2)) { echo "<div class='succes-php'>" . $msgSuccess2 . "</div>"; }
                            ?>
                                <label class="input_wrap" for="mailing">                       
                                    <input id=email name=mailing type=email placeholder="exemple@gmail.com" class="input-form" value="">
                                </label>                           
                                <button class="style-bottom" name="formNewsletter" type="submit">Infolettre</button>
                            </form>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                <section class="container">            
                    <h1><?= $title ?></h1>
                    <p><?= $slug ?></p>
                    <p><?= $content ?></p>
                    <p class="color-03"><a href="/index/#blog"><i class="fas fa-undo"></i>Retour</a></p>     
                </section>


<?php
    require_once folder('/views/layouts/footer.php');
?>