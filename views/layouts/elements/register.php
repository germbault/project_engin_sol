<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
include_once  folder('/functions/data_connector/db-connector.php'); 
include_once folder('/functions/login-register/function-register.php');
include_once folder('/functions/newsletter/newsletter.php');
?>
<!doctype html>
<html class="no-js" lang="fr">

<head>
    <meta charset="utf-8">
    <title>Engin sol</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"></link>

    <meta name="theme-color" content="#fafafa">
</head>

<body>
    <div class="site-container">
        <div class="site-pusher">
            <header>
                <div class="container">
                <div class="user-link">
                    <?php if (isset($_SESSION['auth'])): ?>
                            <span class="color-02">Bienvenue, <strong>(<?php echo $_SESSION["pseudo"]; ?>)</strong></span>
                            <a href="/admin/dashboard.php"><i class="fas fa-th-list"></i>Mon compte&nbsp&nbsp-</a>
                            <a href="/views/layouts/elements/logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a>
                                                        
                                <!-- <?php if ($_SESSION['admin'] == 1) : ?>                         
                                    <li class="edit-profil"><a href="/includes/view-blog/post/panel.php"><span><i class="fas fa-user-cog"></i>Administrateur</span></a></li>
                                <?php endif; ?> -->
                    <?php else: ?>
                    <a href="/views/layouts/elements/login.php"><span><i class="fas fa-user-lock"></i>S'identifier -</span></a>
                    <a href="/views/layouts/elements/register.php"><span><i class="fas fa-user-plus"></i>S'inscrire</span></a>
                    <?php endif; ?>
                </div>
                    <div class="row header-row">
                        <div class="logo">
                            <div class="wrap">                
                                <a href="">
                                    <img class="size-logo" src="/img/svg/logo.svg" alt="logo">
                                </a>              
                            </div>
                        </div>
                        <div class="menu_burger">
                            <a href="#" class="header__icon" id="header__icon"></a>
                        </div>

                        <div>
                            <nav class="col-01">
                                <ul class="menu">                
                                    <li class="remove-cache"><a href="/index/#carrousel">Accueil</a></li>
                                    <li class="menu-lign remove-cache"><a href="/index/#product">Produit</a></li>
                                    <li class="remove-cache"><a href="/index/#blog">Blog</a></li>
                                </ul>
                            </nav>
                            
                            <form id="mailing" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            <?php
                                if (isset($error2)) { echo "<div class='error-php'>" . $error2 . "</div>"; }
                                
                                if (isset($msgSuccess2)) { echo "<div class='succes-php'>" . $msgSuccess2 . "</div>"; }
                            ?>
                                <label class="input_wrap" for="mailing">                       
                                    <input id=email name=mailing type=email placeholder="exemple@gmail.com" class="input-form" value="">
                                </label>                           
                                <button class="style-bottom" name="formNewsletter" type="submit">Infolettre</button>
                            </form>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                            
            <section class="user-content container">
            <h2><span class="ico_user"></span>S'inscrire</h2>  
                <?php
                    if (isset($error)) { echo "<div class='error-php'>" . $error . "</div>"; }
                    
                    if (isset($msgSuccess)) { echo "<div class='succes-php'>" . $msgSuccess . "</div>"; }                    
                ?>
                <form id="register" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                    <input type="hidden" name="register">
                    <table>
                        <tbody>
                            <tr>                               
                                <td>Pseudo<i class="fas fa-user-alt"></i></td>
                                <td><input id=pseudo name=pseudo type=text placeholder="pseudo" class="input-form" value="<?php form_values("pseudo") ?>"></td>
                            </tr>
                            
                            <tr>                               
                                <td>Nom<i class="fas fa-user-alt"></i></td>
                                <td><input id=first_name name=first_name type=text placeholder="Nom" class="input-form" value="<?php form_values("first_name") ?>"></td>
                            </tr>

                            <tr>                                
                                <td>Prénom<i class="fas fa-user-alt"></i></td>
                                <td><input id=last_name name=last_name type=text placeholder="Prenom" class="input-form" value="<?php form_values("last_name") ?>"></td>
                            </tr>

                            <tr>                               
                                <td>Courriel<i class="fas fa-envelope"></i></td>
                                <td><input id=email name=email type=email placeholder="courriel" class="input-form" value="<?php form_values("email") ?>"></td>
                            </tr>

                            <tr>                               
                                <td>Mot de passe<i class="fas fa-unlock"></i></td>
                                <td><input id=pass_word name=pass_word type=pass_word placeholder="Mot de passe"
                                class="input-form"></td>
                            </tr>
                            
                            <tr>
                                <td>confirmation du MPD<i class="fas fa-lock"></i></td>
                                <td><input id=password-confirm name=password_confirm type=password
                                placeholder="Confirmer votre mot de passe" class="input-form"></td>
                            </tr>
                                                                            
                            <tr>
                                <td class="td-buttom" colspan="2">
                                    <button class="user-ajust-buttom" name="formRegister" type="submit">S'inscrire</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="color-03"><a href="/index.php"><i class="fas fa-undo"></i>Retour</a></p>
                </form>
            </section>
       
<?php
    require_once folder('/views/layouts/footer.php');
?>