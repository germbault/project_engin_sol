<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
include_once  folder('/functions/data_connector/db-connector.php'); 
include_once folder('/functions/login-register/function-register.php');
include_once folder('/functions/newsletter/newsletter.php');
?>
<!doctype html>
<html class="no-js" lang="fr">

<head>
    <meta charset="utf-8">
    <title>Engin sol</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"></link>

    <meta name="theme-color" content="#fafafa">
</head>

<body>
    <div class="site-container">
        <div class="site-pusher">
            <header>
                <div class="container">
                <div class="user-link">
                    <?php if (isset($_SESSION['auth'])): ?>
                            <span class="color-02">Bienvenue, <strong>(<?php echo $_SESSION["pseudo"]; ?>)</strong></span>
                            <a href="/admin/dashboard.php"><i class="fas fa-th-list"></i>Mon compte&nbsp&nbsp-</a>
                            <a href="/views/layouts/elements/logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a>
                                                        
                                <!-- <?php if ($_SESSION['admin'] == 1) : ?>                         
                                    <li class="edit-profil"><a href="/includes/view-blog/post/panel.php"><span><i class="fas fa-user-cog"></i>Administrateur</span></a></li>
                                <?php endif; ?> -->
                    <?php else: ?>
                    <a href="/views/layouts/elements/login.php"><span><i class="fas fa-user-lock"></i>S'identifier -</span></a>
                    <a href="/views/layouts/elements/register.php"><span><i class="fas fa-user-plus"></i>S'inscrire</span></a>
                    <?php endif; ?>
                </div>
                    <div class="row header-row">
                        <div class="logo">
                            <div class="wrap">                
                                <a href="">
                                    <img class="size-logo" src="/img/svg/logo.svg" alt="logo">
                                </a>              
                            </div>
                        </div>
                        <div class="menu_burger">
                            <a href="#" class="header__icon" id="header__icon"></a>
                        </div>

                        <div>
                            <nav class="col-01">
                                <ul class="menu">                
                                    <li class="remove-cache"><a href="/index/#carrousel">Accueil</a></li>
                                    <li class="menu-lign remove-cache"><a href="/index/#product">Produit</a></li>
                                    <li class="remove-cache"><a href="/index/#blog">Blog</a></li>
                                </ul>
                            </nav>
                            
                            <form id="mailing" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            <?php
                                if (isset($error2)) { echo "<div class='error-php'>" . $error2 . "</div>"; }
                                
                                if (isset($msgSuccess2)) { echo "<div class='succes-php'>" . $msgSuccess2 . "</div>"; }
                            ?>
                                <label class="input_wrap" for="mailing">                       
                                    <input id=email name=mailing type=email placeholder="exemple@gmail.com" class="input-form" value="">
                                </label>                           
                                <button class="style-bottom" name="formNewsletter" type="submit">Infolettre</button>
                            </form>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                <section class="user-content container">
                    <h2><span class="ico_user"></span>Se connecter</h2>
                    <?php
                        if (isset($error1)) { echo "<div class='error-php'>" . $error1 . "</div>"; }
                        
                        if (isset($msgSuccess1)) { echo "<div class='succes-php'>" . $msgSuccess1 . "</div>"; }
                    ?>          
                    <form id="login" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                        <input type="hidden" name="login">
                        <table>
                            <tbody>
                                <tr>                              
                                    <td>Nom d'utilisateur<i class="fas fa-user-alt"></i></td>
                                    <td><input id=pseudo_login name=pseudo_login type=text placeholder="Saisissez votre pseudo"
                                    class="input-form"></td>
                                </tr>

                                <tr>
                                    <td>Mot de passe<i class="fas fa-unlock"></i></span></td>
                                    <td><input id=pass_word_login name=pass_word_login type=password placeholder="Mot de passe"
                                    class="input-form"></td>
                                </tr>                                  
                                                                                
                                <tr>
                                    <td class="td-buttom" colspan="2">
                                    <button class="user-ajust-buttom" name="formLogin" type="submit">Se connecter</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="user-link">
                            <a href="/views/layouts/elements/register.php" class="style-link"><i class="fas fa-undo"></i>Vous n'ête pas inscrit ?</a>
                        </div>
                        <div class="psw">
                            <span>Vous avez oublié votre <a href="#" class="style-link">mot de passe?</a></span>
                        </div>
                        <p class="color-03"><a href="/index.php"><i class="fas fa-undo"></i>Retour</a></p>
                    </form>
                </section>
       
<?php
    require_once folder('/views/layouts/footer.php');
?>