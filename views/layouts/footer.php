        </main>
        <footer>
            <div class="container">
                <div class="row middle">
                    <div class="grid">
                        <h2>Contactez-nous</h2>
                        <p>175 Chemin des pionniers,O</br>Cap-St-Ignace</br>G0R 1H0</p>
                        <p>germbault@gmail.com</p>
                        <p>Phone: (418) 000-000</br>Fax: (418) 000-000</p>
                    </div>
                
                    <div class="grid">                
                        <h2>Suivez-nous</h2>
                        <ul class="social-list">
                            <li><a class="fab fa-facebook-f" href="#"></a></li>
                            <li><a class="fab fa-rss" href="#"></a></li>
                            <li><a class="fab fa-twitter" href="#"></a></li>
                            <li><a class="fab fa-google-plus-g" href="#"></a></li>
                        </ul>                                                        
                    </div>
                </div>
                <div class="copyrigth">                            
                <p>Copyright @ 2020 Engine Sol .Tous droits réservés</p>                                                            
                </div>
            </div>
            <button onclick="topFunction()" id="scroll-top" title="Go to top">Top</button>
        </footer>
        <div class="site-cache" id="site-cache"></div>
    </div>
</div>

    <script src="/js/vendor/modernizr-3.7.1.min.js"></script>
   
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-3.5.1.min.js"><\/script>')</script>
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>
    

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
  </div>
</body>
</html>