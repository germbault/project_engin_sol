<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php'); 
    
    include_once  folder('/vendor/autoload.php');
    // Utiliser faker pour générer des données aléatoire pour les tests de développement
    $faker = Faker\Factory::create('fr_FR');


    $conn = connect();

    // Permet de vider la table (post)
    // Et d'insérer de fausse donnée pour le blog
    $conn->exec('TRUNCATE TABLE post');

    try {

        for ($i=0; $i < 3; $i++) { 
            $conn->exec("INSERT INTO post SET title='{$faker->sentence()}', slug='{$faker->slug}', created_at='{$faker->date} {$faker->time}' , content='{$faker->paragraphs(rand(3,15), true)} '");
        }
        
    } catch ( PDOException $e ) {
        throw new PDOException($e->getMessage(),(int)$e->getCode());
    }

    // Ensuite exécuter dans le navigateur pour remplir la base de donnée '' http://http://engin.sol.io/commands/fill.php '' Important

    