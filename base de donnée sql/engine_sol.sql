-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 29 juin 2020 à 06:57
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `engine_sol`
--

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`, `created_at`, `updated_at`) VALUES
(9, 'germbault@hotmail.com', '2020-06-27 01:40:46', '2020-06-27 01:40:46'),
(10, 'germbault@otmail.com', '2020-06-28 02:56:32', '2020-06-28 02:56:32');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `title`, `slug`, `content`, `created_at`) VALUES
(1, 'Ab ducimus blanditiis quibusdam sed.', 'mollitia-perspiciatis-animi-enim-officia-explicabo', 'Aut ut rerum qui sunt sit rerum voluptas quaerat. Cumque voluptatem est et. Voluptatum tempora omnis facilis porro sunt nesciunt et.\n\nAsperiores sint dolorem sed quaerat et soluta eos adipisci. Maiores possimus enim et non qui earum sint. Blanditiis est ullam quam fugiat.\n\nSed corporis sint vel saepe ducimus et quia. Dolorem animi nemo autem qui aut minus vel. Ab dolorum minima velit recusandae.\n\nNecessitatibus qui molestias voluptatem accusamus et incidunt. Impedit eveniet culpa saepe dolorem sit est dolores. Nulla quia aut ea recusandae. Eius sed numquam est aut repudiandae quibusdam. Autem nemo et fuga in dolorem mollitia.\n\nSoluta doloribus repudiandae praesentium sint libero. Provident pariatur molestias doloribus laudantium. Neque quos inventore eum voluptates cumque. Iusto et soluta quia laboriosam quos rerum excepturi.\n\nEaque natus vel ipsam deserunt neque ducimus. Velit at natus et sed voluptatem. Suscipit unde est unde dolores vel deserunt laboriosam vel. Excepturi eveniet et commodi dolorem perspiciatis est incidunt.\n\nNemo laborum omnis in neque. Temporibus et eos et ut et. Sit natus nulla ullam in modi voluptatem. Earum impedit sint aut assumenda praesentium quo.\n\nRepellat qui veritatis quia qui minus odio. Sapiente et optio accusantium sed. Adipisci ut fugit aliquid commodi natus. ', '1980-05-30 03:02:39'),
(2, 'Cupiditate eius consequatur animi harum ex.', 'quas-et-voluptatum-nam-perferendis-nisi-modi-qui-saepe', 'Possimus veniam fuga tempore nihil illo iusto repellendus. Aut facilis in odit fugit sint cumque sed nemo. Corrupti corporis laudantium voluptatibus.\n\nEnim quia iure dignissimos earum. Debitis est beatae iure. Sit vel pariatur tempore repudiandae praesentium. Veritatis quia aspernatur voluptates nihil nostrum eveniet. Rerum placeat eligendi quidem soluta consequatur ipsa mollitia.\n\nEnim tenetur sit et neque enim. Cumque sint debitis natus at perspiciatis. Est est blanditiis odit voluptas ea sed sunt. Sequi expedita accusamus maxime voluptas.\n\nExpedita ipsa sint reprehenderit aut totam et. Voluptatem explicabo delectus qui rem at. Et quia repellat quibusdam libero ea expedita consectetur accusantium.\n\nBeatae sequi modi repellat et impedit. Hic aut blanditiis amet. Minus cupiditate facere eum est necessitatibus. Inventore aut et praesentium eos. Dolor saepe non aliquam expedita ut rem. ', '1983-05-01 16:07:45'),
(3, 'Et cumque et est est impedit accusamus quidem illo.', 'cum-nemo-modi-dolor-consectetur-est-consequatur-non-et', 'Assumenda enim omnis vel corporis ex. Expedita rerum quia accusantium. Veniam quia earum rerum dicta. Illum ea corrupti non est consequatur ut debitis.\n\nMolestiae harum soluta quis qui voluptas. Nisi et qui dolores alias et nesciunt culpa.\n\nRecusandae hic est enim magni dignissimos illo. Ratione rerum voluptate quis asperiores molestias totam delectus. Tenetur explicabo qui quia corrupti. Neque ut est et similique.\n\nEligendi fuga dolores nulla ea in cupiditate aspernatur. Consequuntur modi soluta laudantium. Quas maxime eligendi accusantium minus odit.\n\nReprehenderit commodi iusto quia impedit quo maxime nobis. Consequuntur est quo perspiciatis et ducimus officia nam. Consequatur voluptas officiis autem tempora rerum cum deleniti unde. Et sunt a in ullam.\n\nFugiat voluptatem excepturi debitis commodi ex incidunt. Eum quasi ea sit. Maiores sapiente et vel reiciendis id. ', '1987-05-13 06:13:27');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass_word` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_admin` smallint(1) DEFAULT NULL,
  `avatar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `first_name`, `last_name`, `email`, `pass_word`, `created_at`, `updated_at`, `is_admin`, `avatar`) VALUES
(1, 'bob', 'gaetan', 'carol', 'fgfgfgf@ff.ca', '$2y$10$VB9s6rFeM1Ycn9wAHEQMc.dtoFyfHdBNTeYK9WdbK5bdl6XgqRO4K', '2020-06-23 18:10:04', '2020-06-29 00:02:38', 1, '1.jpg'),
(3, 'germbault', 'Germain', 'Thibault', 'germbault@il.com', '$2y$10$oglf6IlMUzJHchnuJch20.hq6/Q1z7NJ9R3AO1cgren08kB/rERlq', '2020-06-27 23:30:06', '2020-06-29 00:10:29', NULL, '3.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
