<?php

function connect(){
    $serveur = "localhost";
    $dbname = "engine_sol";
    $user = "root";
    $password = "";

    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => FALSE,
    ];

    try {
        return new PDO("mysql:host=$serveur;dbname=$dbname", $user, $password, $options);
    } catch ( PDOException $e ) {
        throw new PDOException($e->getMessage(),(int)$e->getCode());
    }
}