<?php
$error2 = null;
$msgSuccess2 = null;

function validate_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validate_data($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['formNewsletterUnsubscribe'])) { 
    $email = validate_data(validate_email($_POST['mailingUnsubscribe']));
    
    try {
        if(!empty($email)) {              
            if ($email) {
                $conn = connect();
                $reqmail = $conn->prepare("SELECT * FROM newsletter WHERE email = ?");
                $reqmail->execute(array($email));
                $mailexist = $reqmail->rowCount();
                if ($mailexist == 1) {                
                    
                        $pdo = $conn->prepare('DELETE FROM newsletter WHERE email = ?');
                        $pdo->execute(array($email));                   
                        $msgSuccess2 = "Votre désinscription à bien été effectué !";
                    
                } else {
                    $error2 = "Vous n'êtes pas inscrit à la Newsletter..";
                }   
            } else {
                $error2 = " Votre adresse mail n'est pas valide !";
            }                     
        } else {    
            $error2 = "Le champ courriel dois être complété !";        
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}