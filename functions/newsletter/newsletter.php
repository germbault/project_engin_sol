<?php
$error2 = null;
$msgSuccess2 = null;

// Pour l'inscription à la newsletter
if (isset($_POST['formNewsletter'])) { 
    $email = validate_data(validate_email($_POST['mailing']));
    
    try {
        if(isset($email)) { 
            if(!empty($email)) {              
                if ($email) {
                    $conn = connect();
                    $reqmail = $conn->prepare("SELECT * FROM newsletter WHERE email = ?");
                    $reqmail->execute(array($email));
                    $mailexist = $reqmail->rowCount();
                    if ($mailexist == 0) {                
                        
                            $pdo = $conn->prepare("INSERT INTO newsletter (email) VALUES(:email)");
        
                            $pdo->execute(array(                                                        
                                ':email' =>    $_POST["mailing"],                                                                                 
                            ));
                            $msgSuccess2 = "Votre inscription à bien été créé !";
                        
                    } else {
                        $error2 = "Vous êtes déjà inscrit à la Newsletter !";
                    }   
                } else {
                    $error2 = " Votre adresse mail n'est pas valide !";
                }                     
            } else {    
                $error2 = "Le champ courriel dois être complété !";        
            }
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}