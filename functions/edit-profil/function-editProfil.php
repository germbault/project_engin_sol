<?php
$error = null;
$msgSuccess = null;
$error1 = null;
$msgSuccess1 = null;
$error2 = null;
$msgSuccess2 = null;

function validate_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validate_data($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// editer le profil de l'utilisateur
if (isset($_POST['editFormUser'])) {
    $editPseudo = validate_data($_POST['editPseudo']);         
    $editFirst_name = validate_data($_POST['editFirst_name']);
    $editLast_name = validate_data($_POST['editLast_name']);
    $editEmail = validate_data(validate_email($_POST['editEmail']));

    try {
        if(isset($editPseudo) and !empty($editPseudo) and $editPseudo != $user["pseudo"]) {
            if (preg_match("/^[a-zA-Z0-9 -]+$/",$editPseudo)) {
                $reqpseudo = $conn->prepare("SELECT * FROM user WHERE pseudo = ?");
                $reqpseudo->execute(array($editPseudo));
                $pseudoexist = $reqpseudo->rowCount();
                if ($pseudoexist == 0) {

                    $insertPseudo =  $conn->prepare("UPDATE user SET pseudo = ? WHERE id = ?"); 
                    $insertPseudo->execute(array($editPseudo, $_SESSION['auth']));
                    $msgSuccess = "Votre pseudo a bien été mis a jour !";

                } else {    
                    $error = "Pseudo déja utilisée !";        
                }
            } else {    
                $error = "Pseudo invalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
            }  
        }

        if(isset($editFirst_name) and !empty($editFirst_name) and $editFirst_name != $user["first_name"]) {
            if (preg_match("/^[a-zA-Z0-9 -]+$/",$editFirst_name)) {
                $insertFirst_name =  $conn->prepare("UPDATE user SET first_name = ? WHERE id = ?"); 
                $insertFirst_name->execute(array($editFirst_name, $_SESSION['auth']));
                $msgSuccess = "Votre nom a bien été mis a jour !";
            } else {    
                $error = "Nom invalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
            }  
        }
        
        if(isset($editLast_name) and !empty($editLast_name) and $editLast_name != $user["last_name"]) {
            if (preg_match("/^[a-zA-Z0-9 -]+$/",$editLast_name)) {
                $insertLast_name =  $conn->prepare("UPDATE user SET last_name = ? WHERE id = ?"); 
                $insertLast_name->execute(array($editLast_name, $_SESSION['auth']));
                $msgSuccess = "Votre prénom a bien été mis a jour !";
            } else {    
                $error = "Prénom invalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
            } 
        }
        
        if(isset($editEmail) and !empty($editEmail) and $editEmail != $user["email"]) {
            if ($editEmail) {
                $reqmail = $conn->prepare("SELECT * FROM user WHERE email = ?");
                $reqmail->execute(array($editEmail));
                $mailexist = $reqmail->rowCount();
                if ($mailexist == 0) {

                    $insertMail =  $conn->prepare("UPDATE user SET email = ? WHERE id = ?"); 
                    $insertMail->execute(array($editEmail, $_SESSION['auth']));
                    $msgSuccess = "Votre courriel a bien été mis a jour !";

                } else {    
                    $error = "Adresse email déja utilisée !";        
                }
            } else {    
                $error = " Votre adresse mail n'est pas valide !";        
            } 
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}

// Éditer le mot de passe de l'utilisateur
if (isset($_POST['editFormPassword'])) {        
    $editPass_word = $_POST["editPass_word"];
    $editPassword_confirm = $_POST["editPassword_confirm"];

    try {
        if(isset($editPass_word) AND isset($editPassword_confirm)) {
            if(!empty($editPass_word) AND !empty($editPassword_confirm)) {
                if ($editPass_word == $editPassword_confirm) {                                            
                    $hashed_password = password_hash($editPass_word, PASSWORD_BCRYPT);
                   
                    $insertMdp =  $conn->prepare("UPDATE user SET pass_word = ? WHERE id = ?"); 
                    $insertMdp->execute(array($hashed_password, $_SESSION['auth']));
                    $msgSuccess1 = "Votre mot de passe a bien été mis a jour !";

                } else {
                    $error1 = " Vos mots de passe ne correspondent pas !";
                }
            }
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}

// Permet d'ajouter un avatar a l'utilisateur
if (isset($_FILES['avatar']) AND !empty($_FILES['avatar']['name'])) {

    $maxSize = 2097152; // taille de 2 mégaoctect maximun
    $extentionValidate = array('jpg', 'jpeg', 'gif', 'png');
    if ($_FILES['avatar']['size'] <= $maxSize) {

        $extentionUpload = strtolower(substr(strrchr($_FILES['avatar']['name'], '.'), 1));
        if (in_array($extentionUpload, $extentionValidate)) {
            $path = "avatar/". $_SESSION['auth'].".".$extentionUpload;
            $result = move_uploaded_file($_FILES['avatar']['tmp_name'], $path);
            if ($result) {

                $insertAvatar = $conn->prepare("UPDATE user SET avatar = :avatar WHERE id = :id");
                $insertAvatar->execute(array(
                    'avatar' => $_SESSION['auth'].".".$extentionUpload,
                    'id' => $_SESSION['auth']
                ));
                $msgSuccess2 = "Votre photo de profil a bien été importé !";

            } else {
                $error2 = " Erreur, l'importation de votre photo de profil a echoué !";
            }
        } else {
            $error2 = " Votre photo de profil doit être au format (jpg, jpeg, gif ou png) !";
        }
    } else {
        $error2 = " Votre photo de profil ne doit pas dépasser 2 mo !";
    }
}
