<?php
session_start();
$error = null;
$error1 = null;
$msgSuccess = null;
$msgSuccess1 = null;

function form_values($first_name) {
    echo (isset($_POST[$first_name]) ? $_POST[$first_name] : "");
}

function validate_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validate_data($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['formRegister'])) { 
    $pseudo = validate_data($_POST['pseudo']);
    $first_name = validate_data($_POST['first_name']);
    $last_name = validate_data($_POST['last_name']);
    $email = validate_data(validate_email($_POST['email']));
    $password1 = $_POST["pass_word"];
    $password2 = $_POST["password_confirm"];

    try {
        if(isset($_POST['pseudo']) AND isset($first_name) AND isset($last_name) AND isset($email) AND isset($password1) AND isset($password2)) {
            if(!empty($pseudo) AND !empty($first_name) AND !empty($last_name) AND !empty($email) AND !empty($password1) AND !empty($password2)) {
                if (preg_match("/^[a-zA-Z0-9 -]+$/",$first_name) AND preg_match("/^[a-zA-Z0-9 -]+$/",$last_name)) {        
                    if ($email) {
                        $conn = connect();
                        $reqmail = $conn->prepare("SELECT * FROM user WHERE email = ?");
                        $reqmail->execute(array($email));
                        $mailexist = $reqmail->rowCount();
                        if ($mailexist == 0) {                
                            if ($password1 == $password2) {                                            
                                $hashed_password = password_hash($password1, PASSWORD_BCRYPT);
                                
                                $pdo = $conn->prepare("INSERT INTO user (pseudo, first_name, last_name, email, pass_word, avatar) 
                                            VALUES(:pseudo, :first_name, :last_name, :email, :pass_word, :avatar )");

                                $pdo->execute(array(
                                    ':pseudo' => $_POST["pseudo"],
                                    ':first_name' => $_POST["first_name"],
                                    ':last_name' =>  $_POST["last_name"],                                
                                    ':email' =>      $_POST["email"],                                
                                    ':pass_word' =>   $hashed_password,
                                    ':avatar' =>      'default.png'
                                                           
                                ));
                                $msgSuccess = "Votre compte a bien été créé !";

                            } else {
                                $error = " Vos mots de passe ne correspondent pas !";
                            }
                        } else {
                            $error = "Adresse email déja utilisée !";
                        }   
                    } else {
                        $error = " Votre adresse mail n'est pas valide !";
                    }
                } else {    
                    $error = "Nom ou prénom invalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
                }               
            } else {    
                $error = "Tous les champs doivent être complétés !";        
            }
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}

if (isset($_POST["formLogin"])) {
    $pseudoconnect = htmlspecialchars($_POST["pseudo_login"]);
    $password = $_POST["pass_word_login"];
    try {
        if (isset($pseudoconnect) AND isset($password)) { 
            if (!empty($pseudoconnect) AND !empty($password)) {            
                $conn = connect();                     
                $requser = $conn->prepare("SELECT * FROM user WHERE pseudo = ?");                                     
                $requser->execute(array($pseudoconnect));
                $user = $requser->fetch();
                $userexist = $requser->rowCount();
                if ($userexist == 1) {
                    
                    $_SESSION["pseudo"] = $user["pseudo"];
                    $_SESSION["auth"] = $user["id"];
                    $_SESSION["pseudo"] = $user["first_name"];
                    $_SESSION["email"] = $user["email"];
                    $_SESSION["pass_word"] = $user["pass_word"];
                    $_SESSION["admin"] = $user["is_admin"];
                    header("Location: /admin/dashboard.php");
                    exit();
        
                } else {
                    $error1 = "Mauvais pseudo ou mot de passe !";
                }                               
                
            } else {
                $error1 = "Tous les champs doivent être complété !";
            }
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }   
}

   

   
    
