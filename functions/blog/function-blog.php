<?php 
$error = null;
$msgSuccess = null;

function form_values($article_title) {
    echo (isset($_POST[$article_title]) ? $_POST[$article_title] : "");
}

function validate_data($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['formNewBlog'])) {         
    $title = validate_data($_POST['article_title']);
    $slug = validate_data($_POST['article_slug']);
    $content = validate_data($_POST['article_content']);
    
    try {
        if(isset($_POST['article_title'], $_POST['article_slug'], $_POST['article_content'])) {
            if(!empty($title) AND !empty($slug) AND !empty($content)) {
                if (preg_match("/^[a-zA-Z0-9 -]+$/",$title)) { 
                    if (preg_match("/^[a-z0-9]+(?:-[a-z0-9]+)*$/",$slug)) {     
                        if (strlen($content) > 5) {

                                $conn = connect();              
                                $pdo = $conn->prepare("INSERT INTO post (title, slug, content) 
                                            VALUES(:title, :slug, :content)");

                                $pdo->execute(array(
                                    ':title' => $_POST["article_title"],
                                    ':slug' =>  $_POST["article_slug"],
                                    ':content' => $_POST["article_content"]
                                                                
                                ));
                                $msgSuccess = "Votre article a bien été ajouté !"; 
                    
                        } else {
                            $error = " Votre contenu doit contenir plus de 5 caractères !";
                        }
                    } else {    
                        $error = "Le slug doit être une combinaison de plusieurs mots séparés par un tiret";        
                    }
                } else {    
                    $error = "Le titre doit être valide: ( majuscule, minuscule et espace seulement {caractère spéciaux non accepté} )";        
                }               
            } else {    
                $error = "Tous les champs doivent être complétés !";        
            }
        }    
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}


if (isset($_GET['edit']) AND !empty($_GET['edit'])) {
    $edit_id = $_GET['edit'];

    $conn = connect();
    $edit_article = $conn->prepare('SELECT * FROM post WHERE id = ?');
    $edit_article->execute(array($edit_id));

    if ($edit_article->rowcount() == 1) {
        $edit_article = $edit_article->fetch();
    } else {
        die("Erreur: l'article concerné n'existe pas...");
    }
}

// Permet de mettre a jour les articles du blog
if (isset($_POST['formEditBlog'])) {         
    $title = validate_data($_POST['edit_title']);
    $slug = validate_data($_POST['edit_slug']);
    $content = validate_data($_POST['edit_content']);
    
    try {
        if(isset($_POST['edit_title'], $_POST['edit_slug'], $_POST['edit_content'])) {
            if(!empty($title) AND !empty($slug) AND !empty($content)) {
                if (preg_match("/^[a-zA-Z0-9 -]+$/",$title)) { 
                    if (preg_match("/^[a-z0-9]+(?:-[a-z0-9]+)*$/",$slug)) {       
                        if (strlen($content) > 5) {

                                $conn = connect();              
                                $update = $conn->prepare("UPDATE post SET title = ?, slug = ?, content = ? WHERE id = ?");                           
                                $update->execute(array($title, $slug, $content, $edit_id));                                                                                        
                                $msgSuccess = "Votre article a bien été modifié !"; 
                    
                        } else {
                            $error = " Votre contenu doit contenir plus de 5 caractères !";
                        }
                    } else {    
                        $error = "Le slug doit être une combinaison de plusieurs mots séparés par un tiret";        
                    }
                } else {    
                    $error = "Le titre doit être valide: ( majuscule, minuscule et espace seulement {caractère spéciaux non accepté} )";       
                }               
            } else {    
                $error = "Tous les champs doivent être complétés !";        
            }
        }    
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}