<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!doctype html>
<html class="no-js" lang="fr">

<head>
<meta charset="utf-8">
    <title>Engin sol-Tableau de bord</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/normalize.css">
    <!-- <link rel="stylesheet" href="/css/main.css"> -->
    <link rel="stylesheet" href="/css/dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"></link>

    <meta name="theme-color" content="#fafafa">
</head>

<body>
    <div class="site-container">
        <div class="site-pusher">
            <div class="row-dashboard">
                <div class="logo-dashboard">                                
                    <a href=""><img src="/img/svg/logo.svg" alt="logo"></a>                             
                </div>
                <div class="menu_burger">
                    <a href="#" class="header__icon" id="header__icon"></a>
                </div>
                <div class="top-right">
                    <p><a href="/index.php"><i class="fas fa-undo"></i>Revenir sur le site</a></p>            
                    <p>Bonjour, (<?php echo $_SESSION["pseudo"]; ?>)
                        <?php if ($_SESSION['admin'] == 1) : ?>
                            <span class="color-02">admin</span>
                        <?php endif; ?></p>
                    <p><a href="/views/layouts/elements/logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a></p>
                </div>                   
            </div>
            <header id="header-dashboard">             
                <div id="avatar">
                <?php if (!empty($_SESSION['auth'])) : ?>
                    <img src="/admin/user/avatar/<?php echo $_SESSION['auth']; ?>" alt="avatar" />
                    <p>Mon avatar</p>
                <?php endif; ?>
                
                </div>        
                 <nav class="menu-vertical">           
                    <ul>            
                        <li class="current_page_item"><a href="/admin/dashboard.php" title="dashboard"><i class="fas fa-tachometer-alt"></i>Tableau de bord</a></li>
                        <li><a href="/admin/newsletter/newsletter.php" title="newsletter"><i class="fas fa-newspaper"></i>Newsletter</a></li>
                        <li><a href="/admin/user/profil.php" title="user"><i class="fas fa-users-cog"></i>User</a></li>
                         <?php if ($_SESSION['admin'] == 1) : ?> 
                            <li><a href="/admin/view-blog/panel.php" title="post"><i class="fas fa-blog"></i>Post</a></li>
                        <?php endif; ?>                        
                    </ul>
                </nav>
            </header>
            
            <main>
                