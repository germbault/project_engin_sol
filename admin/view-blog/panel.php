<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php'); 
    require_once folder('/admin/layouts/header.php');
    

        // Requête pour récuprer les articles de la bd
        $conn = connect();
        $query = $conn->query('SELECT * FROM post ORDER BY created_at DESC');
        $posts = $query->fetchAll();

        if (isset($_SESSION['admin']) == 1) {
    
    ?>
            
    <section class="container"> 
            <h1>Mon panel (liste d'article)</h1>           
            <table class="content-blog">
                <thead class="head-bg">
                    <th>#</th>
                    <th>Titre</th>
                    <th>Actions</th>
                </thead>
                <tbody class="tbody-border">
                    <?php foreach($posts as $post): ?>
                    <tr class="flex-row">
                        <td class="color-text-o1">#<?= $post['id'] ?></td>
                        <td class="color-text-o1"><a href="/admin/view-blog/article.php?id=<?= $post['id'] ?>" class="hover-link"><?= $post['title'] ?></a></td> <!--titre de l'article-->
                        <td class="end">
                            <a href="/admin/view-blog/news.php" class="create-bottom">Créer </a>
                            <a href="/admin/view-blog/edit.php?edit=<?= $post['id'] ?>" class="edit-bottom">Éditer</a>
                            <a href="/admin/view-blog/delete.php?id=<?= $post['id'] ?>" class="delete-bottom" onclick="return confirm('Voulez vous vraiment effectuer cette action ?')">                                   
                                Supprimer
                            </a>
                        </td> <!--différent bouton-->
                    </tr>
                    <?php endforeach ?>                      
                </tbody>
            </table>
            <p class="color-03"><a href="/admin/dashboard.php"><i class="fas fa-undo"></i>Retour</a></p>      
    </section>
    <?php } else { ?>        
        <script>window.location = 'http://engin.sol.io/views/layouts/elements/register.php'; </script>  
<?php
    }
    require_once folder('/admin/layouts/footer.php');
?>