<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php');
    include_once folder('/functions/blog/function-blog.php'); 
    require_once folder('/admin/layouts/header.php');  
    
    if (isset($_SESSION['admin']) == 1) {

?>
            
    <section class="content container">
        <h2><span class="ico_user"></span>Modifier l'article</h2>
        <?php
            if (isset($error)) {
                echo "<div class='error-php'>" . $error . "</div>";
            }
            
            if (isset($msgSuccess)) {
                echo "<div class='succes-php'>" . $msgSuccess . "</div>";
            }
        ?>
        <form id="#" action="#" method="POST">
            <table>
                <tbody>
                    <tr>
                        <td>Titre<i class="fas fa-edit"></i></td>
                        <td><input class="input-table" name="edit_title" type="text" placeholder="Entrez votre titre" value=""></td>
                    </tr>

                    <tr>
                        <td>Slug<i class="fas fa-edit"></i></span></td>
                        <td><input class="input-table" name="edit_slug" type="text" placeholder="Entrez votre slug" value=""></td>
                    </tr> 
                    
                    <tr>
                        <td>contenu<i class="fas fa-envelope"></i></td>
                        <td><textarea class="input-table" name="edit_content" id="comment" placeholder="Entrez votre contenu ici!" 
                                cols="30" rows="10"></textarea>
                    </tr>
                                                                                
                    <tr>
                        <td class="td-buttom" colspan="2">
                        <button class="ajust-buttom" name="formEditBlog" type="submit">Enregistrer</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="color-03"><a href="/admin/view-blog/panel.php"><i class="fas fa-undo"></i>Retour</a></p>
        </form>
    </section>    
    <?php } else { ?>        
        <script>window.location = 'http://engin.sol.io/views/layouts/elements/register.php'; </script>
<?php
    }
    require_once folder('/admin/layouts/footer.php');
?> 