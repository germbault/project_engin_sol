<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php');

if (isset($_GET['id']) AND !empty($_GET['id'])) {
    $delete_id = $_GET['id'];

    $conn = connect();              
    $delete = $conn->prepare("DELETE FROM post WHERE id = ?");                           
    $delete->execute(array($delete_id));
    
    header("Location: http://engin.sol.io/admin/view-blog/panel.php");
}