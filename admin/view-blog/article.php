<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php'); 
    require_once folder('/admin/layouts/header.php');
    
    // Permet d'afficher la totalité de l'article à partir du lien dans une nouvelle page
    if (isset($_GET['id']) AND !empty($_GET['id'])) {
        $get_id = htmlspecialchars($_GET['id']);

        $conn = connect();
        $article = $conn->prepare("SELECT * FROM post WHERE id = ?");
        $article->execute(array($get_id));

        if ($article->rowcount() == 1) {
            $article = $article->fetch();
            $title = $article['title'];
            $slug = $article['slug'];
            $content = $article['content'];
        }else {
            die("Cet article n'existe pas !");
        }
    }else {
        die('Erreur');
    }

    if (isset($_SESSION['admin']) == 1) {

?>

    <main>
        <section class="container">            
            <h1><?= $title ?></h1>
            <p><?= $slug ?></p>
            <p><?= $content ?></p>
            <p class="color-03"><a href="/admin/view-blog/panel.php"><i class="fas fa-undo"></i>Retour</a></p>     
        </section>
    </main>
    <?php } else { ?>        
        <script>window.location = 'http://engin.sol.io/views/layouts/elements/register.php'; </script>   
<?php
    }
    require_once folder('/admin/layouts/footer.php');
?>