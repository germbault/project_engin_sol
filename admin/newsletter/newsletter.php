<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php');
    include_once  folder('/functions/newsletter/unsubscribe.php'); 
    require_once folder('/admin/layouts/header.php');
    
    if (isset($_SESSION['auth'])) {

?>

<section class="user-content container">
    <p>Pour vous désinscrire de l'infolettre, remplissez le formulaire ci-dessous.</p>
    <h2><span class="ico_user"></span>Me désinscrire de l'infolettre</h2>  
    <?php
        if (isset($error2)) { echo "<div class='error-php'>" . $error2 . "</div>"; }
        
        if (isset($msgSuccess2)) { echo "<div class='succes-php'>" . $msgSuccess2 . "</div>"; }
    ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
        <table>
            <tbody>
                <tr>                               
                    <td>Courriel<i class="fas fa-envelope"></i></td>
                    <td><input id=email name=mailingUnsubscribe type=email placeholder="courriel" class="input-form"></td>
                </tr>
                                                                
                <tr>
                    <td class="td-buttom" colspan="2">
                        <button class="user-ajust-buttom" name="formNewsletterUnsubscribe" type="submit">Désinscrire</button>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="color-03"><a href="/admin/dashboard.php"><i class="fas fa-undo"></i>Retour</a></p>
    </form>
    <?php } else { ?>        
        <script>window.location = 'http://engin.sol.io/views/layouts/elements/register.php'; </script>
<?php
    }
    require_once folder('/admin/layouts/footer.php');
?> 