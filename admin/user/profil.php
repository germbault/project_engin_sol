<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/functions/tools/folder-function.php';
    include_once  folder('/functions/data_connector/db-connector.php'); 
    require_once folder('/admin/layouts/header.php');
    
    if (isset($_SESSION['auth'])) {

        // Pour afficher les informations dans les champs
        $conn = connect();
        $requser = $conn->prepare("SELECT * FROM user WHERE id = ?");
        $requser->execute(array($_SESSION['auth']));
        $user = $requser->fetch();  

        include_once folder('/functions/edit-profil/function-editProfil.php');
?>

<section class="user-content container">

    <p>* Choisisser votre photo de profil.</p>
    <h2><span class="ico_user"></span>Éditer mon avatar</h2>  
    <?php
        if (isset($error2)) { echo "<div class='error-php'>" . $error2 . "</div>"; }
        
        if (isset($msgSuccess2)) { echo "<div class='succes-php'>" . $msgSuccess2 . "</div>"; }
    ?>
    <form id="edit_avatar" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" enctype="multipart/form-data">
        <table>
            <tbody>
                <tr>                               
                    <td>Ordinateur<i class="fas fa-desktop"></i></td>
                    <td><input id=user-avatar name=avatar type=file placeholder="Votre avatar" class="" value=""></td>
                </tr>
                                                                                
                <tr>
                    <td class="td-buttom" colspan="2">
                        <p>VOTRE AVATAR NE DOIT PAS FAIRE PLUS DE 150 X 150 PIXELS ET 2Mo</p>
                        <button class="user-ajust-buttom"  type="submit">Enregistrer</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

    <p class="top-block">* Pour mettre a jour votre profil et tous les renseignements liés a votre compte, veillez compléter le formulaire ci-dessous.</p>
    <h2><span class="ico_user"></span>Éditer mon profil</h2>  
    <?php
        if (isset($error)) { echo "<div class='error-php'>" . $error . "</div>"; }
        
        if (isset($msgSuccess)) { echo "<div class='succes-php'>" . $msgSuccess . "</div>"; }
    ?>
    <form id="edit_user" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
        <table>
            <tbody>
                <tr>                               
                    <td>Pseudo<i class="fas fa-user-edit"></i></td>
                    <td><input id=pseudo name=editPseudo type=text placeholder="Pseudo" class="input-form" value="<?= $user["pseudo"]; ?>"></td>
                </tr>
                
                <tr>                               
                    <td>Nom<i class="fas fa-user-edit"></i></td>
                    <td><input id=first_name name=editFirst_name type=text placeholder="Nom" class="input-form" value="<?= $user["first_name"]; ?>"></td>
                </tr>

                <tr>                                
                    <td>Prénom<i class="fas fa-user-edit"></i></td>
                    <td><input id=last_name name=editLast_name type=text placeholder="Prenom" class="input-form" value="<?= $user["last_name"]; ?>"></td>
                </tr>

                <tr>                               
                    <td>Courriel<i class="fas fa-envelope"></i></td>
                    <td><input id=email name=editEmail type=email placeholder="courriel" class="input-form" value="<?= $user["email"]; ?>"></td>
                </tr>
                                                                
                <tr>
                    <td class="td-buttom" colspan="2">
                        <button class="user-ajust-buttom" name="editFormUser" type="submit">Enregistrer</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

    <h2><span class="ico_user"></span>Éditer votre mot de passe</h2>  
    <?php
        if (isset($error1)) { echo "<div class='error-php'>" . $error1 . "</div>"; }
        
        if (isset($msgSuccess1)) { echo "<div class='succes-php'>" . $msgSuccess1 . "</div>"; }
    ?>
    <form id="edit_mdp" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
        <table>
            <tbody>
                <tr>                               
                    <td>Nouveau mot de passe<i class="fas fa-unlock"></i></td>
                    <td><input id=pass_word name=editPass_word type=password placeholder="Mot de passe"
                    class="input-form"></td>
                </tr>
                
                <tr>
                    <td>confirmation du MPD<i class="fas fa-lock"></i></td>
                    <td><input id=password-confirm name=editPassword_confirm type=password
                    placeholder="Confirmer votre mot de passe" class="input-form"></td>
                </tr>
                                                                
                <tr>
                    <td class="td-buttom" colspan="2">
                        <button class="user-ajust-buttom" name="editFormPassword" type="submit">Enregistrer</button>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="color-03"><a href="/admin/dashboard.php"><i class="fas fa-undo"></i>Retour</a></p>
    </form>
</section>
<?php } else { ?>        
        <script>window.location = 'http://engin.sol.io/views/layouts/elements/register.php'; </script>
<?php
    }
    require_once folder('/admin/layouts/footer.php');
?> 