/*
                FENÊTRE MODAL close
=========================================================*/

var modal = document.getElementById('activator');

document.body.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


/*
                      SCROLL top
=========================================================*/


var mybutton = document.getElementById("scroll-top");

window.onscroll = function () { scrollFunction() };

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

/*
                 CAROUSSEL GALLERY 
=========================================================*/

var caroussel = {

    nbSlide: 0,
    nbCurrent: 1,
    elemCurrent: null,
    elem: null,
    timer: null,

    init: function (elem) {
        this.nbSlide = elem.find(".slide-gallery").length;

        // créer la pagination
        elem.append('<div class="navigation"></div>');
        for (var i = 1; i <= this.nbSlide; i++) {
            elem.find(".navigation").append("<span>" + i + "</span>");
        }
        elem.find(".navigation span").click(function () { caroussel.gotoSlide($(this).text()); })

        // Initialisation du caroussel
        this.elem = elem;
        elem.find(".slide-gallery").hide();
        elem.find(".slide-gallery:first").show();
        this.elemCurrent = elem.find(".slide-gallery:first");
        this.elem.find(".navigation span:first").addClass("active");

        // Création du timer
        this.timer = window.setInterval("caroussel.next()", 5000);
    },

    gotoSlide: function (num) {
        if (num == this.nbCurrent) { return false; }

        /* Animation en fadeIn/fadeOut */
        //this.elemCurrent.fadeOut();
        //this.elem.find("#slide" + num).fadeIn();
        /* Fin */

        /* Animation en slide */
        var sens = 1;
        if (num < this.nbCurrent) { sens = -1; }
        var cssStart = { "left": sens * this.elem.width() };
        var cssEnd = { "left": -sens * this.elem.width() };
        this.elem.find("#slide" + num).show().css(cssStart);
        this.elem.find("#slide" + num).animate({ "top": 0, "left": 0 }, 500);
        this.elemCurrent.animate(cssEnd, 500);
        /* Fin */

        this.elem.find(".navigation span").removeClass("active");
        this.elem.find(".navigation span:eq(" + (num - 1) + ")").addClass("active");
        this.nbCurrent = num;
        this.elemCurrent = this.elem.find("#slide" + num);
    },

    next: function () {
        var num = this.nbCurrent + 1;
        if (num > this.nbSlide) {
            num = 1;
        }
        this.gotoSlide(num);
    },

    prev: function () {
        var num = this.nbCurrent - 1;
        if (num < 1) {
            num = this.nbSlide;
        }
        this.gotoSlide(num);
    },
}

jQuery(document).ready(function ($) {

    // Initialisation du caroussel 
    caroussel.init($("#caroussel"));

    // Disparition des boutons svg 
    $(".svg-wrapper").click(function () {
        $(".shape").animate({
            height: 'toggle'
        });
    });

    $(".svg-wrapper1").click(function () {
        $(".shape1").animate({
            height: 'toggle'
        });
    });

    $(".svg-wrapper2").click(function () {
        $(".shape2").animate({
            height: 'toggle'
        });
    });

    $(".close, .cancelbtn").click(function () {
        $(".shape, .shape1, .shape2").animate({
            height: 'show'
        });
    });

    /* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#header__icon').click(function (e) {
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    /* Masquer le menu si on clique sur le cache */
    $('#site-cache, .remove-cache').click(function (e) {
        $('body').removeClass('with--sidebar');
    })

    // Apparition du logo
    $(window).on('load', function () {
        var scale = 1;
        setTimeout(function () {
            scale = scale == 1 ? 1 : 1
            $('.wrap').css('transform', 'scale(' + scale + ')')
        }, 100)
    });
});




